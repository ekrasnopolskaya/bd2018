import threading
import mmap
import datetime
import time
import os

countThreads = 3
maxArr = [None] * countThreads
minArr = [None] * countThreads
sumArr = [None] * countThreads

file = open('file.bin', 'rb')
sizeArray = int.from_bytes(file.read(5), byteorder='big', signed=False)

sizeFile = os.stat('file.bin').st_size - 5
sizePart = sizeFile // countThreads // sizeArray * sizeArray
countIterOnPart = sizePart / sizeArray

def foo(nThreads, threadNum):
    Min = 2 ** 32
    Max = 0
    Sum = 0

    fm = mmap.mmap(file.fileno(), 0, access=mmap.ACCESS_READ)
    fm.seek(5 + threadNum * sizePart)

    iter = 0
    while True:
        byteArray = fm.read(sizeArray)

        if not byteArray:
            break

        if (iter == countIterOnPart) and ((threadNum+1) != nThreads):
            break

        for i in range(round(sizeArray / 4)):
            n = int.from_bytes(byteArray[4 * i:4 * i + 4], byteorder='big', signed=False)
            if n > Max:
                Max = n
            if n < Min:
                Min = n
            Sum += n

        iter += 1

    res = [Min, Max, Sum]
    minArr[threadNum] = Min
    maxArr[threadNum] = Max
    sumArr[threadNum] = Sum

    print('Thread {}: {}'.format(threadNum, res))


startTime = time.time()

threads = [None] * countThreads
for i in range(countThreads):
    threads[i] = threading.Thread(target=foo, args=(countThreads, i))
for i in range(countThreads):
    threads[i].start()
for i in range(countThreads):
    threads[i].join()

endTime = time.time()

print('min: ', min(minArr))
print('max: ', max(maxArr))
print('sum: ', sum(sumArr))
print("Elapsed time: {:.3f} sec".format(endTime - startTime))
file.close()
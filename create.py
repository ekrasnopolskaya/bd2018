import math
import random

with open('file.bin', 'wb') as file:

    sizeFile = 2                                            # Gb            2-х гб файл будет создаваться около получаса
    minCountNum = sizeFile * 2**28                          # sizeFile * 1024 * 1024 * 1024 / 4
    countIter = 20
    sizeArray = math.ceil(minCountNum / countIter / 4) * 4  # for a multiplicity of four
    file.write(sizeArray.to_bytes(5, byteorder='big'))
    for i in range(countIter):
        byteArray = bytearray(sizeArray)

        for j in range(sizeArray):
            n =random.randint(0, 2 ** 32)
            byteArray[4*j:4*j+4] = n.to_bytes(4, byteorder='big')

        file.write(byteArray)
        print((i+1)*(100/countIter), "% completed")
    file.close()
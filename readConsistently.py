import time

Min = 2 ** 32
Max = 0
Sum = 0

startTime = time.time()

with open('file.bin', 'rb') as file:
    sizeArray = int.from_bytes(file.read(5), byteorder='big', signed=False)

    while True:
        byteArray = file.read(sizeArray)

        if not byteArray:
            break

        for i in range(round(sizeArray / 4)):
            n = int.from_bytes(byteArray[4*i:4*i+4], byteorder='big', signed=False)
            if n > Max:
                Max = n
            if n < Min:
                Min = n
            Sum += n

endTime = time.time()

print('min: ', Min)
print('max: ', Max)
print('sum: ', Sum)
print("Elapsed time: {:.3f} sec".format(endTime - startTime))
import os
import time

if not os.path.exists("file.bin"):
    startTime = time.time()
    import create
    print("Elapsed time: {:.3f} sec".format(time.time() - startTime))
    print('\n', "---------------------", '\n')

print("Consistent implementation:")
import readConsistently

print('\n', "---------------------", '\n')

print("Parallel implementation:")
import readParallel
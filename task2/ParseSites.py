from bs4 import BeautifulSoup
import requests
import pandas as pd

column_list = ['voters_count',
               'bulletins_count',
               'bulletins_dosr_count',
               'bulletins_give_in',
               'bulletins_give_out',
               'bulletins_pogash',
               'bulletins_perenes',
               'bulletins_stac',
               'bulletins_invalid',
               'bulletins_valid',
               'bulletins_lost',
               'bulletins_unaccounted',
               'baburin',
               'grudinin',
               'zhirinovskiy',
               'putin',
               'sobchak',
               'suraikin',
               'titov',
               'yavlinskiy']

def get_data(pars, tik_soup, region, tik):
    url_subject = tik_soup.find('a', text='сайт избирательной комиссии субъекта Российской Федерации')['href']
    subject_page = requests.get(url_subject)
    tik_soup = BeautifulSoup(subject_page.content, pars)
    table = pd.read_html(str(tik_soup.select('td div table').pop()), header=0).pop()
    if pd.isnull(table.iat[0,1]):
        table.drop(table.columns[1], axis=1, inplace=True)
    table.dropna(inplace=True)
    table = table.transpose(copy=False)
    for i in range(13, 21):
        table[i] = table[i].apply(lambda x: int(str(x).split()[0]))
    table.columns = column_list
    table.index.name = 'uik'
    table.index = table.index.map(lambda x: int(x[5:]))
    table['region'] = region
    table['tik'] = tik
    return table.copy()

parser = 'html.parser'
page = requests.get('http://www.vybory.izbirkom.ru/region/region/izbirkom?action=show&root=1&tvd=100100084849066&vrn=100100084849062&region=0&global=1&sub_region=0&prver=0&pronetvd=null&vibid=100100084849066&type=227')
soup = BeautifulSoup(page.content, parser)
tables = []
for region in soup.select('nobr a'):
    name = region.get_text()
    print("Обрабатывается: ", name)
    region_soup = BeautifulSoup(requests.get(region['href']).content, parser)
    if (region_soup.find('a', text='сайт избирательной комиссии субъекта Российской Федерации') != None):
        tables.append(get_data(parser, region_soup, name, name))
    else:
        for tik in region_soup.select('nobr a'):
            t_url = tik['href']
            t_name = tik.get_text()
            t_page = requests.get(t_url)
            t_soup = BeautifulSoup(t_page.content, parser)
            tables.append(get_data(parser, t_soup, name, t_name))

pd.concat(tables).to_csv('ElectionsData.csv', sep=";", encoding='utf-8')